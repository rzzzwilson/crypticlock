//********************************************
// Interface to a simple library to handle debug printing.
//********************************************

#ifndef _UTILITY_H_
#define _UTILITY_H_

void debugf(const char *fmt, ...);
void abort(const char *fmt, ...);
void printf(char *fmt, ...);
void crash(int num);
void crashX(int num);

#endif
