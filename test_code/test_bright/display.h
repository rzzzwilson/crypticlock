//********************************************
// Interface to a simple library to handle the 8x8 WS2812B display.
// Same-ish API as the SmartClock API.
//********************************************

#ifndef _DISPLAY_H_
#define _DISPLAY_H_


void disp_begin(void);
void disp_set_pin(int pin);
void disp_clear(void);
void disp_show(void);
void disp_orientation(uint8_t orient);
void disp_brightness(int level);
void disp_status(Status stat);
void disp_time(uint8_t hours, uint8_t minutes, int8_t seconds);
void disp_date(uint8_t day, uint8_t month);
void disp_dow(uint8_t day, bool sun_first);
void disp_year(int year);
void disp_flash_status(int repeat, Status first, Status second);
void disp_boot_stage(int stage);
void disp_flash_boot_stage(void);
void disp_face(const char face);
void disp_progress(int num_pix);
void disp_test(int msec);


#endif
