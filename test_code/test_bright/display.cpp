// Interface to a simple library to handle the 8x8 WS2812B display.
// Same-ish API as the SmartClock API.
//
// This software is designed to work with the smartclock display
// with 4 seven segment LED digits, digit 2 (third from the left)
// being inverted.  The software is low level and uses only the
// SPI library.
//********************************************

#include <SPI.h>
#include <Adafruit_NeoPixel.h>

#include "crypticlock.h"
#include "display.h"
#include "pins.h"
#include "utility.h"


//--------------------------------------------------------
// Display orientation.
//--------------------------------------------------------

int Orientation = 0;

//--------------------------------------------------------
// Last displayed bootstage number.
// Needed by the disp_flash_boot_stage() function.
//--------------------------------------------------------

int BootStage = 0;

//--------------------------------------------------------
// overall display brightness.
//--------------------------------------------------------

uint8_t StartBrightness = 32;  // set static pixel

//--------------------------------------------------------
// 8x8 Neopixel stuff.
//--------------------------------------------------------

// LED array sizes
const int LED_X_SIZE = 8;
const int LED_Y_SIZE = 8;
const int NUM_LEDS = LED_X_SIZE * LED_Y_SIZE + 1; // extra 1 for "sacrificial" LED

// the Neopixels data structure
// note that the "data in" pin here is "0" and the pin must
// be reset by a call to disp_setpin()
Adafruit_NeoPixel pixels(NUM_LEDS, 0, NEO_GRB + NEO_KHZ800);

// (X,Y) offset of top-left point in digit, 0x57 -> (5,7)
const uint8_t DigitTL = 0x07;
const uint8_t DigitTR = 0x57;
const uint8_t DigitBL = 0x02;
const uint8_t DigitBR = 0x52;

// macros to decode the above mappings
#define X_POSN(xy)  (xy >> 4)
#define Y_POSN(xy)  (xy & 0x0f)

//*****
// Colours of pixels in the display.
// The values here are set in "disp_brightness()" depending
// on the ambient light level.
//*****

// no colour, display off, never changes
const int ColourOff = pixels.Color(0, 0, 0);

// "status" bits colours
int ColourStatusRed;
int ColourStatusGreen;
int ColourStatusDoW;
int ColourStatusDate;
int ColourStatusYear;
int ColourStatus404;
int ColourStatusOTA;

// boot stage colout
int ColourStageScale;

// various colours when displaying time
int ColourTime;            // time digits
int ColourStatusClock;     // "status" pixels in the middle
int ColourTwiddleFirst;    // first 5 seconds of 20s twiddle
int ColourTwiddleMiddle;   // middle 10 seconds of 20s twiddle
int ColourTwiddleLast;     // last 5 seconds of 20s twiddle

//*****
// Digits for Cistercian display
//*****

// define 3x3 bitmaps of digits for TL, TR, BL and BR positions
typedef struct
{
  const uint8_t rows[3];
} Digit;

// digits for TOP-RIGHT position
const Digit tr_0 PROGMEM = {0x00, 0x00, 0x00};
const Digit tr_1 PROGMEM = {0x07, 0x00, 0x00};
const Digit tr_2 PROGMEM = {0x00, 0x00, 0x07};
const Digit tr_3 PROGMEM = {0x04, 0x02, 0x01};
const Digit tr_4 PROGMEM = {0x01, 0x02, 0x04};
const Digit tr_5 PROGMEM = {0x07, 0x02, 0x04};
const Digit tr_6 PROGMEM = {0x01, 0x01, 0x01};
const Digit tr_7 PROGMEM = {0x07, 0x01, 0x01};
const Digit tr_8 PROGMEM = {0x01, 0x01, 0x07};
const Digit tr_9 PROGMEM = {0x07, 0x01, 0x07};

// digits for TOP-LEFT position
const Digit tl_0 PROGMEM = {0x00, 0x00, 0x00};
const Digit tl_1 PROGMEM = {0x07, 0x00, 0x00};
const Digit tl_2 PROGMEM = {0x00, 0x00, 0x07};
const Digit tl_3 PROGMEM = {0x01, 0x02, 0x04};
const Digit tl_4 PROGMEM = {0x04, 0x02, 0x01};
const Digit tl_5 PROGMEM = {0x07, 0x02, 0x01};
const Digit tl_6 PROGMEM = {0x04, 0x04, 0x04};
const Digit tl_7 PROGMEM = {0x07, 0x04, 0x04};
const Digit tl_8 PROGMEM = {0x04, 0x04, 0x07};
const Digit tl_9 PROGMEM = {0x07, 0x04, 0x07};

// digits for BOTTOM-LEFT position
const Digit bl_0 PROGMEM = {0x00, 0x00, 0x00};
const Digit bl_1 PROGMEM = {0x00, 0x00, 0x07};
const Digit bl_2 PROGMEM = {0x07, 0x00, 0x00};
const Digit bl_3 PROGMEM = {0x04, 0x02, 0x01};
const Digit bl_4 PROGMEM = {0x01, 0x02, 0x04};
const Digit bl_5 PROGMEM = {0x01, 0x02, 0x07};
const Digit bl_6 PROGMEM = {0x04, 0x04, 0x04};
const Digit bl_7 PROGMEM = {0x04, 0x04, 0x07};
const Digit bl_8 PROGMEM = {0x07, 0x04, 0x04};
const Digit bl_9 PROGMEM = {0x07, 0x04, 0x07};

// digits for BOTTOM-RIGHT position
const Digit br_0 PROGMEM = {0x00, 0x00, 0x00};
const Digit br_1 PROGMEM = {0x00, 0x00, 0x07};
const Digit br_2 PROGMEM = {0x07, 0x00, 0x00};
const Digit br_3 PROGMEM = {0x01, 0x02, 0x04};
const Digit br_4 PROGMEM = {0x04, 0x02, 0x01};
const Digit br_5 PROGMEM = {0x04, 0x02, 0x07};
const Digit br_6 PROGMEM = {0x01, 0x01, 0x01};
const Digit br_7 PROGMEM = {0x01, 0x01, 0x07};
const Digit br_8 PROGMEM = {0x07, 0x01, 0x01};
const Digit br_9 PROGMEM = {0x07, 0x01, 0x07};

// digits for each display corner
Digit tr_cisc[] = {tr_0, tr_1, tr_2, tr_3, tr_4, tr_5, tr_6, tr_7, tr_8, tr_9};
Digit tl_cisc[] = {tl_0, tl_1, tl_2, tl_3, tl_4, tl_5, tl_6, tl_7, tl_8, tl_9};
Digit bl_cisc[] = {bl_0, bl_1, bl_2, bl_3, bl_4, bl_5, bl_6, bl_7, bl_8, bl_9};
Digit br_cisc[] = {br_0, br_1, br_2, br_3, br_4, br_5, br_6, br_7, br_8, br_9};

//*****
// Digits for "die" display
//*****

// digits for die values
const Digit die_0 PROGMEM = {0x00, 0x00, 0x00};
const Digit die_1 PROGMEM = {0x00, 0x02, 0x00};
const Digit die_2 PROGMEM = {0x04, 0x00, 0x01};
const Digit die_3 PROGMEM = {0x04, 0x02, 0x01};
const Digit die_4 PROGMEM = {0x05, 0x00, 0x05};
const Digit die_5 PROGMEM = {0x05, 0x02, 0x05};
const Digit die_6 PROGMEM = {0x07, 0x00, 0x07};
const Digit die_7 PROGMEM = {0x07, 0x02, 0x07};
const Digit die_8 PROGMEM = {0x07, 0x05, 0x07};
const Digit die_9 PROGMEM = {0x07, 0x07, 0x07};

// digits for each display corner (for the "die" display, same in all corners)
Digit tr_die[] = {die_0, die_1, die_2, die_3, die_4, die_5, die_6, die_7, die_8, die_9};
Digit tl_die[] = {die_0, die_1, die_2, die_3, die_4, die_5, die_6, die_7, die_8, die_9};
Digit bl_die[] = {die_0, die_1, die_2, die_3, die_4, die_5, die_6, die_7, die_8, die_9};
Digit br_die[] = {die_0, die_1, die_2, die_3, die_4, die_5, die_6, die_7, die_8, die_9};

//*****
// Digits for "binary" display
//*****

// digits for binary values
const Digit bin_0 PROGMEM = {0x00, 0x00, 0x00};
const Digit bin_1 PROGMEM = {0x00, 0x00, 0x01};
const Digit bin_2 PROGMEM = {0x00, 0x00, 0x04};
const Digit bin_3 PROGMEM = {0x00, 0x00, 0x05};
const Digit bin_4 PROGMEM = {0x01, 0x00, 0x00};
const Digit bin_5 PROGMEM = {0x01, 0x00, 0x01};
const Digit bin_6 PROGMEM = {0x01, 0x00, 0x04};
const Digit bin_7 PROGMEM = {0x01, 0x00, 0x05};
const Digit bin_8 PROGMEM = {0x04, 0x00, 0x00};
const Digit bin_9 PROGMEM = {0x04, 0x00, 0x01};

// digits for each display corner (for the "binary" display, same in all corners)
Digit tr_bin[] = {bin_0, bin_1, bin_2, bin_3, bin_4, bin_5, bin_6, bin_7, bin_8, bin_9};
Digit tl_bin[] = {bin_0, bin_1, bin_2, bin_3, bin_4, bin_5, bin_6, bin_7, bin_8, bin_9};
Digit bl_bin[] = {bin_0, bin_1, bin_2, bin_3, bin_4, bin_5, bin_6, bin_7, bin_8, bin_9};
Digit br_bin[] = {bin_0, bin_1, bin_2, bin_3, bin_4, bin_5, bin_6, bin_7, bin_8, bin_9};

//*****
// Pointers to the clockface digits to use
//*****

// point to the set of digits we are going to use, Die by default
Digit *TLDigits = tl_die;
Digit *TRDigits = tr_die;
Digit *BLDigits = bl_die;
Digit *BRDigits = br_die;


//--------------------------------------------------------
// Convert X,Y coordinate to LED serial address.
// Depends on the orientation.
//--------------------------------------------------------

static uint8_t xy2lin(uint8_t x, uint8_t y)
{
  switch (Orientation)
  {
    case 0:
      if (y % 2)  // Y is odd, right to left numbering
      {
        return (LED_X_SIZE - x - 1) + y*LED_Y_SIZE;
      }
        
      // Y is even, left to right numbering
      return x + y*LED_X_SIZE;

    case 1:
      if (x % 2)  // X is odd, top to bottom numbering
      {
        return (LED_Y_SIZE - x - 1)*LED_X_SIZE + y;
      }
        
      // X is even, bottom to top numbering
      return (LED_Y_SIZE - x)*LED_X_SIZE - y - 1;

    case 2:
      if (y % 2)  // Y is odd, right to left numbering
      {
        return (LED_Y_SIZE - y)*LED_Y_SIZE -1 - x;
      }

      // Y is even, left to right numbering
      return x + (LED_Y_SIZE - y - 1)*LED_Y_SIZE;

    case 3:
      if (x % 2)  // X is odd, bottom to top numbering
      {
        return LED_X_SIZE*x + y;
      }

      // x is even, top to bottom numbering
      return LED_X_SIZE*(x + 1) - y - 1;
  }

  return 0;     // return a default orientation
}

//--------------------------------------------------------
// Set a point on the display to on or off.
//     x, y   - coordinates of the point
//     value  - value to set point to, 0 means OFF, else ON
//--------------------------------------------------------

static void disp_point(uint8_t x, uint8_t y, int value)
{
  pixels.setPixelColor(xy2lin(x, y), value);
}

//--------------------------------------------------------
// Set the display clockface.
// Used only by crypticlock.
//
// Point the TL/TR/BL/BR digits pointers to the appropriate data.
//--------------------------------------------------------

void disp_face(const char face)
{
  switch (face)
  {
    case 'D':   // "die" digits
      TLDigits = tl_die;
      TRDigits = tr_die;
      BLDigits = bl_die;
      BRDigits = br_die;
      break;
    case 'C':   // Cistercian digits
      TLDigits = tl_cisc;
      TRDigits = tr_cisc;
      BLDigits = bl_cisc;
      BRDigits = br_cisc;
      break;
    case 'B':   // "bin" digits
      TLDigits = tl_bin;
      TRDigits = tr_bin;
      BLDigits = bl_bin;
      BRDigits = br_bin;
      break;
  }
}

//--------------------------------------------------------
// Twiddle the "working" indicator on the screen.
//
// This twiddle tries to give a rough indication of the seconds.
// The three centre-bottom two columns are split.  The bottom
// two indicate the seconds are in the first 20 seconds in the
// minute.  The next row up for the middle 20 seconds and the
// top two for the last 20 seconds.  In each row the dots are
// green for the first 5 seconds, yellow for the next 10 seconds
// and red for the last 5 seconds of the row.
//--------------------------------------------------------

static void twiddle(uint8_t seconds)
{
  // previous twiddle position
  static uint8_t prev_twiddle_col = 255;
  static uint8_t prev_twiddle_row = 255;

  // turn off previous twiddle dot if set
  if (prev_twiddle_col != 255)
  {
    disp_point(prev_twiddle_col, prev_twiddle_row, ColourOff);
  }

  // determine row to twiddle and seconds remaining
  uint8_t row = seconds / 20;    // determine the row to use
  uint8_t remsecs = seconds - row * 20;

  // set red/green mix from the remaing seconds
  int tcolour;

  if (remsecs < 5)
    tcolour = ColourTwiddleFirst;
  else if (remsecs < 15)
    tcolour = ColourTwiddleMiddle;
  else
    tcolour = ColourTwiddleLast;

  // calculate position we are to use, left or right column
  uint8_t col = (seconds % 2) ? 4 : 3;

  // set the twiddle dot
  disp_point(col, row, tcolour);

  prev_twiddle_col = col;
  prev_twiddle_row = row;
}

//--------------------------------------------------------
// Draw the digit at the given position.
//     posn   - ID of position to draw at in the display
//     digit  - pointer to Digit data to draw
//--------------------------------------------------------

static void draw_digit(uint8_t posn, Digit *digit)
{
  int dx = X_POSN(posn);
  int dy = Y_POSN(posn);

  for (int row = 0; row < 3; ++row)
  {
    uint8_t row_data = digit->rows[row];

    for (int col = 0; col < 3; ++col)
    {
      if (row_data & 0x04)
      {
        disp_point(dx + col, dy - row, ColourTime);
      }
      else
      {
        disp_point(dx + col, dy - row, ColourOff);
      }

      row_data <<= 1;
    }
  }
}

//--------------------------------------------------------
// Show a number of red pixels on the screen.
//
//     num_pix  the number of green pixels to set [1..x]
//
// Clears display and sets pixels in order of strip, 0, 1, 2, ...
// A debug routine.
//--------------------------------------------------------

void disp_progress(int num_pix)
{
  disp_clear();

  for (int pix=0; pix < num_pix; ++pix)
  {
    pixels.setPixelColor(num_pix, ColourStatusRed);
  }

  disp_show();
  delay(1000);
}

//--------------------------------------------------------
// Show the stage number of the boot.
//
//     stage  boot stage number [0..7]
//
// Clears bottom row of display then shows appropriate LED.
//--------------------------------------------------------

void disp_boot_stage(int stage)
{
  // save stage number in case disp_flash_boot_stage() is called
  BootStage = stage;

  // now display the appropriate stage number
  for (int x=0; x < 8; ++x)
  {
    disp_point(x, 0, ColourStageScale);
  }

  for (int x=0; x <= stage; ++x)
  {
    disp_point(x, 0, ColourStatusGreen);
  }

  pixels.show();
  delay(200);
}

//--------------------------------------------------------
// Flash the stage number LED of the boot.
//
// Flashes the LED number stored in the BootStage global.
// disp_boot_stage() sets BootStage.
//--------------------------------------------------------

void disp_flash_boot_stage(void)
{
  for (int i=0; i < 2; ++i)
  {
    disp_point(BootStage, 0, ColourStageScale);
    pixels.show();
    delay(200);
    disp_point(BootStage, 0, ColourStatusGreen);
    pixels.show();
    delay(200);
  }
}

//---------------------------------------------
// Initialize the display.
//---------------------------------------------

void disp_begin(void)
{
  // initialize the LED array
  pixels.begin();
  disp_brightness(StartBrightness);
  pixels.show();
}

//---------------------------------------------
// Set the Din pin for the display.
//
// We want to override the pin used when creating
// the "pixels" object because we REALLY want to define
// that pin in crypticlock.ino.
//---------------------------------------------

void disp_set_pin(int pin)
{
  pixels.setPin(pin);
}

//---------------------------------------------
// Show the display in TEST mode, everything ON.
//     pause  the time in milliseconds to show the test
//
// Does nothing in crypticlock.
//---------------------------------------------

void disp_test(int pause)
{
  int part_pause = pause / 3;
  
  for (int num_pix=0; num_pix < 64; ++num_pix)
  {
    pixels.setPixelColor(num_pix, pixels.Color(3, 0, 0));
  }
  disp_show();
  delay(part_pause);

  for (int num_pix=0; num_pix < 64; ++num_pix)
  {
    pixels.setPixelColor(num_pix, pixels.Color(0, 3, 0));
  }
  disp_show();
  delay(part_pause);

  for (int num_pix=0; num_pix < 64; ++num_pix)
  {
    pixels.setPixelColor(num_pix, pixels.Color(0, 0, 3));
  }
  disp_show();
  delay(part_pause);
  disp_clear();
}

//---------------------------------------------
// Clear the display.
//---------------------------------------------

void disp_clear(void)
{
  pixels.clear();
}

//---------------------------------------------
// Show and changed pixels on the display.
//---------------------------------------------

void disp_show(void)
{
  pixels.show();
}

//---------------------------------------------
// Set the current display orientation.
//---------------------------------------------

void disp_orientation(uint8_t orient)
{
  Orientation = orient;
}

//--------------------------------------------------------
// disp_status() - show a status on the display.
//     status  code for status to be displayed
// Sets pixels on clockface as well as the sacrificial LED.
//--------------------------------------------------------

void disp_status(Status stat)
{
  // show LEDs appropriate to the status
  switch (stat)
  {
    case STATUS_BOOT:
      disp_point(3, 3, ColourStatusRed);
      disp_point(3, 4, ColourStatusRed);
      disp_point(4, 3, ColourStatusRed);
      disp_point(4, 4, ColourStatusRed);
      break;
    case STATUS_CONFIG:
      disp_point(3, 3, ColourStatusRed);
      disp_point(3, 4, ColourStatusRed);
      disp_point(4, 3, ColourStatusRed);
      disp_point(4, 4, ColourStatusRed);
      disp_point(2, 5, ColourStatusRed);
      disp_point(5, 5, ColourStatusRed);
      disp_point(2, 2, ColourStatusRed);
      disp_point(5, 2, ColourStatusRed);
      break;
    case STATUS_CONFIG_WAIT:
      disp_point(3, 3, ColourStatusRed);
      disp_point(3, 4, ColourStatusRed);
      disp_point(4, 3, ColourStatusRed);
      disp_point(4, 4, ColourStatusRed);
      disp_point(2, 5, ColourStatusGreen);
      disp_point(5, 5, ColourStatusGreen);
      disp_point(2, 2, ColourStatusGreen);
      disp_point(5, 2, ColourStatusGreen);
      break;
    case STATUS_CONFIG_WAIT2:
      disp_point(3, 3, ColourStatusGreen);
      disp_point(3, 4, ColourStatusGreen);
      disp_point(4, 3, ColourStatusGreen);
      disp_point(4, 4, ColourStatusGreen);
      disp_point(2, 5, ColourStatusRed);
      disp_point(5, 5, ColourStatusRed);
      disp_point(2, 2, ColourStatusRed);
      disp_point(5, 2, ColourStatusRed);
      break;
    case STATUS_CLOCK:
      disp_point(3, 3, ColourStatusClock);
      //FALLTHROUGH
    case STATUS_DOW:
      disp_point(4, 3, ColourStatusClock);
      //FALLTHROUGH
    case STATUS_DATE:
      disp_point(4, 4, ColourStatusClock);
      // FALLTHROUGH
    case STATUS_YEAR:
      disp_point(3, 4, ColourStatusClock);
      break;
    case STATUS_404:
      disp_point(3, 3, ColourStatus404);
      disp_point(3, 4, ColourStatus404);
      disp_point(4, 3, ColourStatus404);
      disp_point(4, 4, ColourStatus404);
      break;
    case STATUS_OTA:
      // "O"
      disp_point(0, 7, ColourStatusOTA);
      disp_point(1, 7, ColourStatusOTA);
      disp_point(2, 7, ColourStatusOTA);
      disp_point(0, 6, ColourStatusOTA);
      disp_point(2, 6, ColourStatusOTA);
      disp_point(0, 5, ColourStatusOTA);
      disp_point(1, 5, ColourStatusOTA);
      disp_point(2, 5, ColourStatusOTA);
      // "T"
      disp_point(2, 4, ColourStatusOTA);
      disp_point(3, 4, ColourStatusOTA);
      disp_point(4, 4, ColourStatusOTA);
      disp_point(5, 4, ColourStatusOTA);
      disp_point(3, 3, ColourStatusOTA);
      disp_point(3, 2, ColourStatusOTA);
      disp_point(4, 3, ColourStatusOTA);
      disp_point(4, 2, ColourStatusOTA);
      // "A"
      disp_point(5, 2, ColourStatusOTA);
      disp_point(6, 2, ColourStatusOTA);
      disp_point(7, 2, ColourStatusOTA);
      disp_point(5, 1, ColourStatusOTA);
      disp_point(6, 1, ColourStatusOTA);
      disp_point(7, 1, ColourStatusOTA);
      disp_point(5, 0, ColourStatusOTA);
      disp_point(7, 0, ColourStatusOTA);
      break;
    default:
      debugf("set_status: Bad status code: %d\n", stat);
      break;
  }
}

//--------------------------------------------------------
// Set brightness appropriate to the hour.
//
//     level  integer ambient light level, [1..7]
//
// With Adafruit Neopixel we can't control the *overall*
// brightness as we did with fastLED, so we must switch
// colour brightness.
//
// Add ClockData.Brightness to "level" to get brightness.
//--------------------------------------------------------

void disp_brightness(int level)
{
  // "base" used to shorten code below
  int base = level;

  ColourStatusRed = pixels.Color(base, 0, 0);
  ColourStatusGreen = pixels.Color(0, base, 0);
  ColourStatusDoW = pixels.Color(base, base, 0);
  ColourStatusDate = pixels.Color(base, base, base);
  ColourStatusYear = pixels.Color(0, base, base);
  ColourStatus404 = pixels.Color(0, 0, base);
  ColourStatusOTA = pixels.Color(base, 0, base);
  ColourStageScale = pixels.Color(base, 0, 0);
  ColourTime = pixels.Color(0, base, base);
  ColourStatusClock = pixels.Color(base, 0, base);
  ColourTwiddleFirst = pixels.Color(0, base, 0);
  ColourTwiddleMiddle = pixels.Color(base, base, 0);
  ColourTwiddleLast = pixels.Color(base, 0, 0);
}

//--------------------------------------------------------
// disp_flash_status() - Flash a quick error status on display.
//     repeat  number of times to flash
//     first   first status in flash sequence
//     second  second status in flash sequence
//--------------------------------------------------------

void disp_flash_status(int repeat, Status first, Status second)
{
    for (int i = 0; i < repeat; ++i)
    {
      disp_status(first);
      delay(100);
      pixels.show();
      disp_status(second);
      delay(100);
      pixels.show();
    }
}

//--------------------------------------------------------
// Update the display to show the new time.
//     hours, minutes, seconds  - the time to display
//--------------------------------------------------------

void disp_time(uint8_t hours, uint8_t minutes, int8_t seconds)
{
  draw_digit(DigitTR, &TRDigits[hours % 10]);
  draw_digit(DigitTL, &TLDigits[hours / 10]);
  draw_digit(DigitBR, &BRDigits[minutes % 10]);
  draw_digit(DigitBL, &BLDigits[minutes / 10]);

  twiddle(seconds);
}

//---------------------------------------------
// Show a date on the display.
//     day, month  the date to show
//
// Leading zeros on day and month are suppressed.
//---------------------------------------------

void disp_date(uint8_t day, uint8_t month)
{
  draw_digit(DigitTR, &TRDigits[day % 10]);
  draw_digit(DigitTL, &TLDigits[day / 10]);
  draw_digit(DigitBR, &BRDigits[month % 10]);
  draw_digit(DigitBL, &BLDigits[month / 10]);
}

//---------------------------------------------
// Show the day of the week on the display.
//     day        the date to show
//     sun_first  true if Sunday is first, else Monday
//
// Show digit 4 DP if Sunday is first DoW.
//---------------------------------------------

void disp_dow(uint8_t day, bool sun_first)
{
  uint8_t day_digit = day + 1; // assume Sunday is first DoW

  if (!sun_first)
  {
    // Monday is first, raw Sunday (0) is 7, rest unchanged
    day_digit = (day == 0) ? 7 : day;
  }
  
  draw_digit(DigitTR, &BRDigits[day_digit]);

  if (sun_first)
  {
    disp_point(3, 7, ColourTime);
  }
}

//---------------------------------------------
// Show a year on the display.
//     year    the date to show
//---------------------------------------------

void disp_year(int year)
{
  draw_digit(DigitTL, &TRDigits[(year / 1000) % 10]);
  draw_digit(DigitTR, &TLDigits[(year / 100) % 10]);
  draw_digit(DigitBL, &BRDigits[(year / 10) % 10]);
  draw_digit(DigitBR, &BLDigits[(year) % 10]);
}
