//********************************************
// A library to handle orientation data from an external sensor.
//********************************************

#ifndef _ORIENTATION_H_
#define _ORIENTATION_H_

void orient_begin(int sda, int sdl=0);
byte orient_read(void);

#endif
