//********************************************
// A library to sense the ambient light level.
//********************************************

#include "crypticlock.h"
#include "utility.h"
#include "ambient.h"


// save the pin number that we read an analog value from
static int AmbientPin = 0;

//---------------------------------------------
// Initialize the sensor.
//---------------------------------------------

void ambient_begin(int pin)
{
  AmbientPin = pin;
  pinMode(pin, INPUT);
}

//---------------------------------------------
// Get the current light level.
//
// Returns an integer in the range [MinAmbient..MaxAmbient].
//---------------------------------------------

int ambient_read(void)
{
  int ambient_raw = analogRead(AmbientPin);
  int ambient = map(ambient_raw, 0, 300, MinAmbient, MaxAmbient);
  
  debugf("ambient_read: ambient_raw=%d, returning ambient=%d\n",
         ambient_raw, ambient);

  return ambient;
}
