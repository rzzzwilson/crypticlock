//********************************************
// Interface to simple debug printing functions.
//********************************************

#include "crypticlock.h"
#include "display.h"
#include "utility.h"

//--------------------------------------------------------
// Provide a "printf()"-like function, shortens code.
// Only prints if debug is non-zero.
//--------------------------------------------------------

void debugf(const char *fmt, ...)
{
  if (ClockData.Debug)
  {
    va_list ptr;
    char tmp_buff[512];
  
    memset(tmp_buff, 0, sizeof(tmp_buff));
    
    va_start(ptr, fmt);
    vsprintf(tmp_buff,fmt, ptr);
    va_end(ptr);
  
    Serial.print(tmp_buff);
  }
}

//--------------------------------------------------------
// Abort the program.
// Prints printf() style args, then loops forever.
//--------------------------------------------------------

void abort(const char *fmt, ...)
{
  va_list ptr;
  char tmp_buff[512];

  memset(tmp_buff, 0, sizeof(tmp_buff));
  
  va_start(ptr, fmt);
  vsprintf(tmp_buff,fmt, ptr);
  va_end(ptr);

  Serial.println(tmp_buff);

  while (1)
  {
    for (int i = 0; i < 64; ++i)
      disp_progress(i);
  }
}

//--------------------------------------------------------
// Provide a "printf()" function, shortens code.
// Always prints.
//--------------------------------------------------------

void printf(char *fmt, ...)
{
  va_list ptr;
  char tmp_buff[512];

  memset(tmp_buff, 0, sizeof(tmp_buff));
  
  va_start(ptr, fmt);
  vsprintf(tmp_buff,fmt, ptr);
  va_end(ptr);

  Serial.print(tmp_buff);
}
