//********************************************
// Common stuff for the crypticlock.
//********************************************

#ifndef _CRYPTICLOCK_H_
#define _CRYPTICLOCK_H_

#include <Arduino.h>

//--------------------------------------------------------
// Set max/min brightness values and ambient values
//--------------------------------------------------------

const int MinBrightness = 0;    // min/max brightness settings
const int MaxBrightness = 7;

const int MinAmbient = 0;       // min/max ambient values
const int MaxAmbient = 3;

//--------------------------------------------------------
// Status codes, used to show various configurations on display
//--------------------------------------------------------

enum Status {STATUS_BOOT, STATUS_CONFIG, STATUS_CONFIG_WAIT,
             STATUS_CONFIG_WAIT2, STATUS_DOW, STATUS_DATE,
             STATUS_YEAR, STATUS_CLOCK, STATUS_404, STATUS_OTA};

//--------------------------------------------------------
// Define the ClockData structure.
//--------------------------------------------------------

// set sizes of SSID, Password, URL & NTP/TZ server buffers in EEPROM and RAM
const int SizeCredentialsBuffer = 32;   // limits size of SSID & password
const int SizeURLBuffer = 128;          // size of buffer to create URL
const int SizeTimezoneBuffer = 32;      // buffer for timezone name
const int SizeServerBuffer = 64;        // buffer for TZ server url

// struct holding data saved to EEPROM
typedef struct
{
  char SSID[SizeCredentialsBuffer];         // WiFi SSID name
  char Password[SizeCredentialsBuffer];     // password for the SSID
  char Timezone[SizeTimezoneBuffer];        // timezone to use
  char Clockface;                           // sets the clockface to use
  int BootHour;                             // hour when TZ is checked
  char Calendar;                            // 'G' or 'B' - Gregorian or Thai Buddhist
  bool ShowDate;                            // true if we show date
  bool FirstDoWSunday;                      // true if Sunday is first DoW
  int Brightness;                           // display brightness [MinBrightness..MaxBrightness]
  bool Debug;                               // true if DEBUG is on
} EepromData;

// in-memory copy of EEPROM data
extern EepromData ClockData;

// macro for comparing strings more naturally
#define STREQ(a, b) (strcmp((a), (b)) == 0)

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v) (void) (v)

#endif
