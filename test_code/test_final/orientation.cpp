//********************************************
// A library to handle orientation data from an external sensor.
//********************************************

#include <Wire.h>
#include <MPU6050.h>

#include "crypticlock.h"
#include "utility.h"
#include "orientation.h"
#include "pins.h"


//--------------------------------------------------------
// MPU6050 accelerometer
//--------------------------------------------------------

MPU6050 accelgyro;


//---------------------------------------------
// Initialize the sensor.
// 
// The "pin" parameter is ignored here.
//---------------------------------------------

void orient_begin(int sca, int sdl)
{
  // prepare comms for accelerometer
  Wire.begin(sca, sdl);

  // initialize the accelerometer
  accelgyro.initialize();
}

//---------------------------------------------
// Get the current orientation.
//
// Returns an integer with meaning:
//     0  X is UP
//     1  Y is UP
//     2  X is DOWN
//     3  Y is DOWN
//---------------------------------------------

byte orient_read(void)
{
  int16_t ax, ay, az;
  byte result = -1;

  accelgyro.getAcceleration(&ax, &ay, &az);
  
  if (abs(ax) > abs(ay))
    result = (ax > 0) ? 2 : 0;
  else
    result = (ay > 0) ? 3 : 1;

  return result;
}
