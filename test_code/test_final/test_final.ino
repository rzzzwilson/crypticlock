//**********************************************************************
// A cryptic clock using an ESP-12E module, an MPU6050 accelerometer and
// an 8*8 WS2812B display to show the time.
//
// The display can show one of three "clockfaces":
//     * binary digits
//     * cistercian digits
//     * extended "die" faces, 0..9
//
// At a configurable hour the clock reboots which checks for any timezone
// changes, which should handle daylight saving changes.
//
// To configure the clock, hold the CONFIG button down while applying
// power.  The clock will show the "configuring" display.  Use a phone,
// tablet or computer to connect to the "CryptiClock x.y" open access
// point and then point a browser at "http://192.168.4.1".
//
// Updating software is done by selecting "OTA upload" on the config page
// and rebooting.  OTA upload then occurs from the IDE.  If necessary,
// wired programming can be done using the ESP-12E Programmming Box with a
// 6 pin 2mm spacing POGO connection.
//**********************************************************************

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <EEPROM.h>

#include "crypticlock.h"
#include "display.h"
#include "orientation.h"
#include "ambient.h"
#include "utility.h"
#include "pins.h"


//--------------------------------------------------------
// Program name and version.
//--------------------------------------------------------

const char *ProgName = "CryptiClock";

#define VERSION_MAJOR       2
#define VERSION_MINOR       4
#define VERSION_ALL         (VERSION_MAJOR*10 + VERSION_MINOR)

//--------------------------------------------------------
// Set non-zero if a reset of the EEPROM is to be forced.
// Don't forget to set to 0 after compiling!
//--------------------------------------------------------

#define EEPROM_RESET        0

//--------------------------------------------------------
// Define this value non-zero if you want regular NTP updates.
// If not defined the clock is set once at boot, and is never updated.
// If set, the WiFi connection is persistent, else it terminates after boot.
//--------------------------------------------------------

#define NTP_UPDATE          1

//--------------------------------------------------------
// Define this non-zero if you want the restart reason shown.
// Only displays if ClockData.Debug also true.
//--------------------------------------------------------

#define SHOW_RESTART_REASON 1

//--------------------------------------------------------
// Define non-zero if the display is tested at boot.
//--------------------------------------------------------

#define DISPLAY_TEST        1

//--------------------------------------------------------
// Access Point credentials.
// Password not used in this version, access point is open.
//--------------------------------------------------------

const char* config_password = "";   // no password required

//--------------------------------------------------------
// The number of times we scan for WiFi APs
//--------------------------------------------------------

const int NumWifiScans = 3;

//--------------------------------------------------------
// IP address of the device access point used for configuration
//--------------------------------------------------------

IPAddress    apIP(192, 168, 4, 1);  

//--------------------------------------------------------
// define pin for the CONFIG button
//--------------------------------------------------------

const int ConfigButton = GPIO12;  // if pushed at boot time, enter config mode

//--------------------------------------------------------
// define pin for the accelerometer I2C
//--------------------------------------------------------

const int AccelSda = GPIO4;
const int AccelScl = GPIO5;

//--------------------------------------------------------
// define pin for the display Din
//--------------------------------------------------------

const int DispDin = GPIO13;

//--------------------------------------------------------
// define pin for the light sensor
//--------------------------------------------------------

const int LightPin = A0;        // analog voltage on the LDRs

//--------------------------------------------------------
// Buffer to create program name + version string
//--------------------------------------------------------

char ProgNameVersion[32];

//--------------------------------------------------------
// NTP time and TZ stuff.
//--------------------------------------------------------

// URLs for UTC offset from local IP or supplied timezone
const char *IpLookupUrl = "http://ip-api.com/json/?fields=offset";
const char *TzLookupUrl = "http://worldtimeapi.org/api/timezone/%s.txt";

// server for NTP information
const char *NTPServer = "pool.ntp.org";

// stuff for time lookup
WiFiUDP UdpConnection;
NTPClient timeClient(UdpConnection);

WiFiClient client;
HTTPClient http;

const unsigned long NTPUpdateInterval = 1000 * 60 * 5;  // every 5 minutes
long TZOffsetSec = 0;                                   // timezone offset from UTC

// Time "fudge factor" used to move NTP time back/forward in seconds
// +1 moves time forward 1 second, -1 is back
// this is to allow for some "tardiness" in the time retrieved from the NTP servers
const int NtpFudge = 0;

//--------------------------------------------------------
// EEPROM data stuff.
//--------------------------------------------------------

// default settings, these are put into EEPROM if checksum doesn't match
const char* DefaultSSID = "";               // MUST BE CONFIGURED
const char* DefaultPassword = "";           // MUST BE CONFIGURED
const char* DefaultTimezone = "";           // empty means automatic TZ determination
const int DefaultBootHour = 4;              // hour when daylight savings is checked
const char DefaultClockface = 'D';          // "die" clockface
const char DefaultCalendar = 'G';           // Gregorian calendar
const bool DefaultShowDate = true;          // if true show yesr/date/DoW
const bool DefaultFirstDoWSunday = true;    // if true Sunday is first, else Monday
const int DefaultBrightness = MaxBrightness;// max brightness during first config
const bool DefaultDebug = false;            // if true debug prints sent over Serial

// the EEPROM data CRC
unsigned long Checksum = 0;

// in-memory copy of EEPROM data, filled from EEPROM after reboot
EepromData ClockData;

// define start addresses in EEPROM of various fields
const int EepromAddrChecksum = 0;
const int EepromAddrData = EepromAddrChecksum + sizeof(Checksum);

//--------------------------------------------------------
// Buffer for any error message from config page
//--------------------------------------------------------

char ErrorMessage[128] = "";

//*****
// The webserver object and HTML data
//*****

const int MaxSsidLen = 33;    // max length of one SSID string
const int MaxNumSsid = 28;    // max number of SSID strings

ESP8266WebServer server(80);

typedef struct
{
  char ssid[MaxSsidLen+1];
  long rssi;
} APData;     // entry in AvailSSID array

APData AvailSSID[MaxNumSsid];
int NumAvailSSID = 0;

// the web page we serve on connection to 192.168.4.1
char const HTMLStart[] = R"=====(
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>CryptiClock Config</title>
    <style>
      html { font-family: Helvetica;
             display: inline-block;
             margin: 0px auto; text-align: center; }
      body { margin-top: 10px; }
      h1 { color: #444444; margin: 30px auto 30px; }
      h2 { color: #444444; margin-bottom: 50px; }
      h3 { color: #444444; margin-bottom: 50px; }
      .button { display: block; width: 100px; background-color: #1abc9c;
                border: none; color: #eee; padding: 13px 30px;
                text-decoration: none; font-size: 35px; margin: 0px auto 35px;
                cursor: pointer; border-radius: 4px; }
      .center { margin-left: auto; margin-right: auto; }
      p { font-size: xx-small; margin-top: 1px; margin-bottom: 1px; }
      a { font-size: xx-small; margin-top: 1px; margin-bottom: 1px; }
      #ssid       { width:190px; }
      #password   { width:190px; }
      #timezone   { width:190px; }
      #clockface  { width:190px; }
      #boothour   { width:190px; }
      #calendar   { width:190px; }
      #show_date  { width:190px; }
      #first_dow  { width:190px; }
      #brightness { width:190px; }
      #debug      { width:190px; }
    </style>
  </head>
  <body>
)=====";

// Error messages will be placed here

char const SelectSSID[] = R"=====(
    <form action="/save">
      <table class="center">
        <tr>
          <td style="text-align: right;">SSID</td>
            <td>
              <select name="ssid" id="ssid">
)=====";

char const SelectPassword[] = R"=====(
            </select>
        </tr>
        <tr>
          <td style="text-align: right;">Password</td>
)=====";

char const SelectTimeZone[] = R"=====(
        </tr>
        <tr>
          <td style="text-align: right;">Timezone</td>
)=====";

char const SelectClockface[] = R"=====(
        </tr>
        <tr>
          <td>
          </td>
          <td>
            <a>timezones: worldtimeapi.org/api/timezone.txt</a>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">Clockface</td>
          <td>
            <select name="clockface" id="clockface">
)=====";

char const SelectBootHour[] = R"=====(
        </tr>
        <tr>
          <td style="text-align: right;">Reboot hour</td>
          <td>
            <select name="boothour" id="boothour">
)=====";

char const SelectCalendar[] = R"=====(
            </select>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">Calendar</td>
          <td>
            <select name="calendar" id="calendar">
)=====";

char const SelectShowDate[] = R"=====(
            </select>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">Show date</td>
          <td>
            <select name="show_date" id="show_date">
)=====";

char const SelectStartDoW[] = R"=====(
            </select>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">First DoW</td>
          <td>
            <select name="first_dow" id="first_dow">
)=====";

char const SelectBrightness[] = R"=====(
            </select>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">Brightness</td>
          <td>
            <select name="brightness" id="brightness">
)=====";

char const SelectDebug[] = R"=====(
            </select>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">Debug</td>
          <td>
            <select name="debug" id="debug">
)=====";

char const SelectOTA[] = R"=====(
            </select>
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">OTA upload</td>
          <td align="left">
            <input type="checkbox" id="ota" name="ota">
)=====";

char const HTMLEnd[] = R"=====(
          </td>
        </tr>
        <tr>
          <td></td>
          <td><input type="submit" value="Save & Reboot"></td>
        </tr>
      </table>
    </form> 
  </body>
</html>
)=====";

//--------------------------------------------------------
// Display reason for start/restart.
//--------------------------------------------------------

#if SHOW_RESTART_REASON != 0
void show_restart_reason(void)
{
  char *reason = NULL;
  
  switch (ESP.getResetInfoPtr()->reason)
  {
    case REASON_DEFAULT_RST: 
      reason = (char *) "Power on";
      break;
      
    case REASON_WDT_RST:
      reason = (char *) "Hardware Watchdog";     
      break;
      
    case REASON_EXCEPTION_RST:
      reason = (char *) "Exception";      
      break;
      
    case REASON_SOFT_WDT_RST:
      reason = (char *) "Software Watchdog";
      break;
      
    case REASON_SOFT_RESTART: 
      reason = (char *) "Software/System restart";
      break;
      
    case REASON_DEEP_SLEEP_AWAKE:
      reason = (char *) "Deep-Sleep Wake";
      break;
      
    case REASON_EXT_SYS_RST:
      reason = (char *) "External System";
      break;
      
    default:  
      reason = (char *) "Unknown";     
      break;
  }

  printf("Restart because: %s\n", reason);
}
#endif


//--------------------------------------------------------
// Function to handle OTA uploading.
//
// Doesn't return, reboots.
//--------------------------------------------------------

void ota_upload(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(ClockData.SSID, ClockData.Password);
  debugf("Connecting to AP '%s'.", ClockData.SSID);
  
  while (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    delay(1000);
    debugf(".");
  }
  
  ArduinoOTA.begin();

  while (true)
  {
    ArduinoOTA.handle();
    delay(100);
  }

  ESP.restart();
}

#ifdef HEX_DUMP
//--------------------------------------------------------
// Helper for debugdumphex(), prints one line of 16 bytes.
//    base  memory address of start of dump
//--------------------------------------------------------

void debugdumphex_helper(char *base)
{
  if (ClockData.Debug)
  {
    char ascii[17];
  
    memset(ascii, 0, sizeof(ascii));
  
    printf("%08x  ", (unsigned int) base);
  
    for (int i = 0; i < 16; ++i)
    {
      char ch = *(base + i);
  
      printf("%02x ", ch);
      if (isprint(ch))
        *(ascii + i) = ch;
      else
        *(ascii + i) = '.';
    }
    
    printf("  %s\n", ascii);
  }
}

//--------------------------------------------------------
// Print a region of memory in HEX/ASCII.
//    msg   address of header message string
//    base  memory address of start of dump
//    num   number of bytes to dump
//--------------------------------------------------------

void debugdumphex(const char *msg, void *base, int num)
{
  if (ClockData.Debug)
  {
    char *off = (char *) base;
    const char *delim = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                        "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
  
    printf(delim);
    printf("Hex dump: %s\n", msg);
    
    while (num > 0)
    {
      debugdumphex_helper(off);
      num -= 16;
      off += 16;
    }
    
    printf(delim);
  }
}
#endif

//********************************************************
// Code to get/set EEPROM values.
//--------------------------------------------------------
// Calculate the CRC of the data in memory
// This code from: https://www.arduino.cc/en/Tutorial/LibraryExamples/EEPROMCrc
//--------------------------------------------------------

unsigned long eeprom_crc(void)
{
  const unsigned long crc_table[16] =
  { 0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
    0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
  };
  unsigned long crc = ~0L;

  for (unsigned long index = EepromAddrData; index < sizeof(ClockData); ++index)
  {
    crc = crc_table[(crc ^ EEPROM.read(index)) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM.read(index) >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}

//--------------------------------------------------------
// Saves the in-memory data to EEPROM and refreshes the CRC.
//--------------------------------------------------------

void save_eeprom_data(void)
{
  EEPROM.put(EepromAddrData, ClockData);
  
  Checksum = eeprom_crc();
  EEPROM.put(EepromAddrChecksum, Checksum);
  
  EEPROM.commit();
}

//--------------------------------------------------------
// Gets the data in EEPROM and refreshes the RAM copies.
//
// If the EEPROM checksum is invalid, sets EEPROM and
// in-memory copy to default values.
//--------------------------------------------------------

void restore_eeprom_data(void)
{
  // get expected data checksum
  EEPROM.get(EepromAddrChecksum, Checksum);

  // if checksum wrong, initialize EEPROM
  // reset forced if EEPROM_RESET is defined non-zero
#if EEPROM_RESET == 0
  if (Checksum != eeprom_crc())
#endif
  {
    printf("Checksum error, initializing EEPROM\n");

    // force values to the default state
    strcpy(ClockData.SSID, DefaultSSID);
    strcpy(ClockData.Password, DefaultPassword);
    strcpy(ClockData.Timezone, DefaultTimezone);
    ClockData.BootHour = DefaultBootHour;
    ClockData.Clockface = DefaultClockface;
    ClockData.Calendar = DefaultCalendar;
    ClockData.ShowDate = DefaultShowDate;
    ClockData.FirstDoWSunday = DefaultFirstDoWSunday;
    ClockData.Brightness = DefaultBrightness;
    ClockData.Debug = DefaultDebug;

    // and save initialized data (also updates EEPROM checksum)
    save_eeprom_data();
  }

  // get data stored in EEPROM
  EEPROM.get(EepromAddrData, ClockData);
}

//********************************************************
// Handlers for the configuration web server.
//********************************************************
// Send HTML showing selections for various things
//--------------------------------------------------------

void send_error_message(void)
{
  if (strlen(ErrorMessage))
  {
    server.sendContent("<h5><font color=\"red\">");
    server.sendContent(ErrorMessage);
    server.sendContent("</font></h3>");
  }
}

void send_set_ssid(void)
{
  char buff[200];

  debugf("ClockData.SSID='%s', NumAvailSSID=%d\n", ClockData.SSID, NumAvailSSID);

  if (NumAvailSSID)
  {
    for (int i = 0; i < NumAvailSSID; ++i)
    {
      char const *selected = "";
      
      if (STREQ(AvailSSID[i].ssid, ClockData.SSID))
      {
        selected = " selected";
      }
      
      sprintf(buff, "<option value=\"%s\"%s>%s</option>\n",
                    AvailSSID[i].ssid, selected, AvailSSID[i].ssid);
      server.sendContent(buff);
    }
  }
  else if (strlen(ClockData.SSID))
  {
    sprintf(buff, "<option value=\"%s\" selected>%s</option>\n",
                  ClockData.SSID, ClockData.SSID);
    server.sendContent(buff);
  }
}

void send_set_password(void)
{
  if (strlen(ClockData.Password))
  {
    server.sendContent("<td><input name=\"password\" value=\"\"placeholder=\"password is set\"></td>");
  }
  else
  {
    server.sendContent("<td><input name=\"password\" value=\"\"></td>");
  }
}

void send_set_timezone(void)
{
  char buff[128];

  if (strlen(ClockData.Timezone) == 0)
  {
    sprintf(buff, "<td><input type=\"text\" name=\"timezone\""
                  "value=\"\" placeholder=\"auto if not specified\"><br></td>\n");        
  }
  else
  {
    sprintf(buff, "<td><input type=\"text\" name=\"timezone\" value=\"%s\"><br></td>\n",
                  ClockData.Timezone);    
  }
  
  server.sendContent(buff);
}

void send_selected_clockface(void)
{
  if (ClockData.Clockface == 'B')
    server.sendContent("<option value=\"binary\" selected>Binary</option>");
  else
    server.sendContent("<option value=\"binary\">Binary</option>");

  if (ClockData.Clockface == 'C')
    server.sendContent("<option value=\"cistercian\" selected>Cistercian</option>");
  else
    server.sendContent("<option value=\"cistercian\">Cistercian</option>");
    
  if (ClockData.Clockface == 'D')
    server.sendContent("<option value=\"die\" selected>Die</option");
  else
    server.sendContent("<option value=\"die\">Die</option");
}

void send_selected_boothour(void)
{
  if (ClockData.BootHour == 23)
    server.sendContent("<option value=\"23\" selected>2300</option>");
  else
    server.sendContent("<option value=\"23\">2300</option>");

  if (ClockData.BootHour == 0)
    server.sendContent("<option value=\"0\" selected>0000</option>");
  else
    server.sendContent("<option value=\"0\">0000</option>");

  if (ClockData.BootHour == 1)
    server.sendContent("<option value=\"1\" selected>0100</option>");
  else
    server.sendContent("<option value=\"1\">0100</option>");

  if (ClockData.BootHour == 2)
    server.sendContent("<option value=\"2\" selected>0200</option>");
  else
    server.sendContent("<option value=\"2\">0200</option>");

  if (ClockData.BootHour == 3)
    server.sendContent("<option value=\"3\" selected>0300</option>");
  else
    server.sendContent("<option value=\"3\">0300</option>");

  if (ClockData.BootHour == 4)
    server.sendContent("<option value=\"4\" selected>0400</option>");
  else
    server.sendContent("<option value=\"4\">0400</option>");

  if (ClockData.BootHour == 5)
    server.sendContent("<option value=\"5\" selected>0500</option>");
  else
    server.sendContent("<option value=\"5\">0500</option>");

  if (ClockData.BootHour == 6)
    server.sendContent("<option value=\"6\" selected>0600</option>");
  else
    server.sendContent("<option value=\"6\">0600</option>");
}

void send_selected_calendar(void)
{
  if (ClockData.Calendar == 'G')
    server.sendContent("<option value=\"G\" selected>Gregorian</option>");
  else
    server.sendContent("<option value=\"G\">Gregorian</option>");

  if (ClockData.Calendar == 'B')
    server.sendContent("<option value=\"B\" selected>Thai Buddhist</option>");
  else
    server.sendContent("<option value=\"B\">Thai Buddhist</option>");
}

void send_selected_showdate(void)
{
  if (ClockData.ShowDate)
    server.sendContent("<option value=\"T\" selected>Yes</option>");
  else
    server.sendContent("<option value=\"T\">Yes</option>");

  if (!ClockData.ShowDate)
    server.sendContent("<option value=\"F\" selected>No</option>");
  else
    server.sendContent("<option value=\"F\">No</option>");
}

void send_selected_firstdow(void)
{
  if (ClockData.FirstDoWSunday)
    server.sendContent("<option value=\"S\" selected>Sunday</option>");
  else
    server.sendContent("<option value=\"S\">Sunday</option>");

  if (!ClockData.FirstDoWSunday)
    server.sendContent("<option value=\"M\" selected>Monday</option>");
  else
    server.sendContent("<option value=\"M\">Monday</option>");
}

void send_selected_brightness(void)
{
  char buff[128];

  for (int b = MaxBrightness; b >= MinBrightness; --b)
  {
    if (ClockData.Brightness == b)
    {
      if (b == MaxBrightness)
        sprintf(buff, "<option value=\"%d\" selected>%d - brightest</option>", b, b);
      else if (b == MinBrightness)
        sprintf(buff, "<option value=\"%d\" selected>%d - dimmest</option>", b, b);
      else
        sprintf(buff, "<option value=\"%d\" selected>%d</option>", b, b);
    }
    else
    {
      if (b == MaxBrightness)
        sprintf(buff, "<option value=\"%d\">%d - brightest</option>", b, b);
      else if (b == MinBrightness)
        sprintf(buff, "<option value=\"%d\">%d - dimmest</option>", b, b);
      else
        sprintf(buff, "<option value=\"%d\">%d</option>", b, b);
    }
    
    server.sendContent(buff);
  }
}

void send_selected_debug(void)
{
  if (ClockData.Debug)
  {
    server.sendContent("<option value=\"T\" selected>On</option>\n");
    server.sendContent("<option value=\"F\">Off</option>\n");
  }
  else
  {
    server.sendContent("<option value=\"T\">On</option>\n");
    server.sendContent("<option value=\"F\" selected>Off</option>\n");
  }
}

//--------------------------------------------------------
// Handle initial connect.
//--------------------------------------------------------

void handle_OnConnect()
{
  char buff[64];

  server.setContentLength(CONTENT_LENGTH_UNKNOWN);
  server.send(200, "text/html", HTMLStart);

  // send the name+version
  sprintf(buff, "<h1>%s Config</h1>\n", ProgNameVersion);
  server.sendContent(buff);
  
  send_error_message();   // send errors, if any
  
  server.sendContent(SelectSSID);
  send_set_ssid();
  
  server.sendContent(SelectPassword);
  send_set_password();  // show there is a password if one

  server.sendContent(SelectTimeZone);
  send_set_timezone();
  
  server.sendContent(SelectClockface);
  send_selected_clockface();
  
  server.sendContent(SelectBootHour);
  send_selected_boothour();
  
  server.sendContent(SelectCalendar);
  send_selected_calendar();

  server.sendContent(SelectShowDate);
  send_selected_showdate();

  server.sendContent(SelectStartDoW);
  send_selected_firstdow();
  
  server.sendContent(SelectBrightness);
  send_selected_brightness();
  
  server.sendContent(SelectDebug);
  send_selected_debug();
  
  server.sendContent(SelectOTA);
  // there is nothing to do here
  
  server.sendContent(HTMLEnd);
}

void handle_Save()
{
  bool do_ota = false;  // flag - 'true' if we want to do OTA upload
  
  // reset any error message
  strcpy(ErrorMessage, "");

  // assume ClockData.Debug off
  ClockData.Debug = false;

  // set config variables from GET request values
  for (int i = 0; i < server.args(); ++i)
  {
    const char *paramname = server.argName(i).c_str();
    const char *paramvalue = server.arg(i).c_str();

    debugf("handle_Save: paramname='%s', paramvalue='%s'\n", paramname, paramvalue);

    if (STREQ(paramname, "ssid"))
    {
      strcpy(ClockData.SSID, paramvalue);
    }
    else if (STREQ(paramname, "password"))
    {
      if (strlen(paramvalue) > 0)
        strcpy(ClockData.Password, paramvalue);
    }
    else if (STREQ(paramname, "timezone"))
    {
      strcpy(ClockData.Timezone, paramvalue);
    }
    else if (STREQ(paramname, "clockface"))
    {
      if (STREQ(paramvalue, "binary"))
        ClockData.Clockface = 'B';
      else if (STREQ(paramvalue, "cistercian"))
        ClockData.Clockface = 'C';
      else if (STREQ(paramvalue, "die"))
        ClockData.Clockface = 'D';
    }
    else if (STREQ(paramname, "boothour"))
    {
      ClockData.BootHour = atoi(paramvalue);
    }
    else if (STREQ(paramname, "calendar"))
    {
      if (STREQ(paramvalue, "G"))
        ClockData.Calendar = 'G';
      else if (STREQ(paramvalue, "B"))
        ClockData.Calendar = 'B';
    }
    else if (STREQ(paramname, "show_date"))
    {
      ClockData.ShowDate = (STREQ(paramvalue, "T"));
    }
    else if (STREQ(paramname, "first_dow"))
    {
      ClockData.FirstDoWSunday = (STREQ(paramvalue, "S"));
    }
    else if (STREQ(paramname, "brightness"))
    {
      ClockData.Brightness = atoi(paramvalue);
    }
    else if (STREQ(paramname, "debug"))
    {
      ClockData.Debug = (STREQ(paramvalue, "T"));
    }
    else if (STREQ(paramname, "ota"))
    {
      do_ota = true;
    }
    else
    {
      printf("Unrecognized ID: '%s', value='%s'\n", paramname, paramvalue);
    }
  }
  
  // save any changed data
  save_eeprom_data();

  // do OTA upload if requested
  if (do_ota)
  {
    server.send(200, "text/html", "<h1>CryptiClock OTA uploading</h1>");
    debugf("OTA upload...");
    disp_orientation(orient_read());
    disp_clear();
    disp_status(STATUS_OTA);
    disp_show();
    delay(1000);
    ota_upload();
    
    // doesn't return
  }

  // otherwise reboot
  server.send(200, "text/html", "<h1>Rebooting CryptiClock ...</h1>");
  debugf("Rebooting...");
  disp_status(STATUS_BOOT);
  disp_show();
  delay(1000);
  
  ESP.restart();
}

void handle_NotFound()
{
  server.send(404, "text/plain", "Not found");
}

//--------------------------------------------------------
// Check if SSID is already in the AvailSSID table.
//
//     ssid  string holding SSID
//
// Returns "true" if the SSID is in the table, else "false".
//--------------------------------------------------------

bool ssid_in_table(const char *ssid, int next_slot)
{
  for (int i = 0; i < next_slot; ++i)
  {
    if (STREQ(ssid, AvailSSID[i].ssid))
    {
      return true;
    }
  }

  return false;
}

//--------------------------------------------------------
// Fill AvailSSID with unique SSID+RSSI values.
//
//     next_slot  index of next free slot in table
//
// Returns index of the *new* next free slot.
//--------------------------------------------------------

int fill_AvailSSID(int next_slot)
{
  delay(100);
  
  int num_ssid = WiFi.scanNetworks();

  // Got some SSID+RSSI data, fill AvailSSID table
  for (int i = 0; i < num_ssid; ++i)
  {
    String ssid = WiFi.SSID(i);

    // if the SSID already in the table, try next SSID
    if (ssid_in_table(ssid.c_str(), next_slot))
    {
      continue;
    }

    // SSID is new, add to table
    strcpy(AvailSSID[next_slot].ssid, ssid.c_str());
    AvailSSID[next_slot].rssi = WiFi.RSSI(i);
    ++next_slot;

    if (next_slot >= MaxNumSsid)
    {
      printf("Too many SSIDs, got %d, max is %d\n", next_slot, MaxNumSsid);
      while (1)
        ;
    }
  }

  return next_slot;
}

//--------------------------------------------------------
// Comparison function - sort by RSSI descending
//--------------------------------------------------------

int sort_rssi(const void *cmp1, const void *cmp2)
{
  // Need to cast the void* to APData*
  APData *a = (APData *) cmp1;
  APData *b = (APData *) cmp2;

  return b->rssi - a->rssi;
}

//--------------------------------------------------------
// Initialize the config server.
//--------------------------------------------------------

void init_config_server(void)
{
  // scan available networks
  // https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/scan-examples.html
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  NumAvailSSID = 0;

  for (int i = 0; i < NumWifiScans; ++i)
  {
    NumAvailSSID = fill_AvailSSID(NumAvailSSID);
  }
  
  // sort seen SSIDs by signal strength
  qsort(AvailSSID, NumAvailSSID, sizeof(AvailSSID[0]), sort_rssi);

  // create an Access Point for configuration
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(ProgNameVersion, config_password);

  server.on("/", handle_OnConnect);
  server.on("/save", handle_Save);
  server.onNotFound(handle_NotFound);
  server.begin();
  
  printf("Config server available on SSID '%s', IP=%d.%d.%d.%d\n",
         ProgNameVersion, apIP[0], apIP[1], apIP[2], apIP[3]);
}

//********************************************************
// Timezone code.
//--------------------------------------------------------
// Return timezone offset in seconds.
//     payload - string of data from lookup
//
// Data is from a TZ guess from IP data.
// The string we are looking for is:
//     ^....,"offset":25200,...$
//                    ^^^^^  offset in seconds
//--------------------------------------------------------

long analyze_iptz(char *payload)
{
  char *keystr = (char *) "\"offset\":";

  debugf("analyze_iptz: payload:\n%s\n", payload);
#ifdef HEX_DUMP
//  debugdumphex("analyze_iptz: payload:", payload, strlen(payload));
#endif
  debugf("analyze_iptz: keystr='%s'\n", keystr);

  // look for key string
  char *index = strstr(payload, keystr);
  if (index == NULL)
  {
    abort("analyze_iptz: payload='%s'\nCouldn't find substring '%s'!?\n", payload, keystr);
  }
  index += strlen(keystr);   // point at first non-match char
  debugf("analyze_iptz: sign, index=>'%s'\n", index);

  // convert string into seconds offset from UTC
  bool neg = (*index == '-');
  if (neg)
  {
    ++index;
  }
  debugf("analyze_iptz: neg=%s\n", (neg) ? "true" : "false");
  
  int utc_offset_sec = atoi(index);
  if (neg)
  {
    utc_offset_sec = -utc_offset_sec;
  }
  debugf("analyze_iptz: utc_offset_sec=%d, hours offset=%.2f\n",
         utc_offset_sec, (float) utc_offset_sec / 3600);

  return utc_offset_sec;
}

//--------------------------------------------------------
// Return timezone offset in seconds.
//     payload - string of data from lookup
//     keystr  - the string that immediately precedes offset
//
// Data is from a TZ lookup.
// The string we are looking for is:
//     ^....,"offset":25200,...$
//                    ^^^^^  offset in seconds
//--------------------------------------------------------

long analyze_tztz(char *payload)
{
  int utc_offset_sec;
  char *keystr = (char *) "utc_offset: ";

  debugf("analyze_tztz: payload:\n%s\n\n", payload);
#ifdef HEX_DUMP
//  debugdumphex("payload:", payload, strlen(payload));
#endif
  debugf("analyze_tztz: keystr='%s'\n", keystr);

  // look for key string
  char *index = strstr(payload, keystr);
  if (index == NULL)
  {
    abort("analyze_tztz: payload='%s'\nCouldn't find substring '%s'!?\n", payload, keystr);
  }
  index += strlen(keystr);   // point at first non-match char
  debugf("analyze_tz_info: sign, index=>'%s'\n\n", index);
  debugf("*index='%c'\n", *index);

  // convert string (eg, "-04:00" or "+09:30") into seconds offset from UTC
  bool neg = (*index == '-');
  if (*index == '+' || *index == '-')
  {
    ++index;
  }
  debugf("analyze_tztz: neg=%s\n", (neg) ? "true" : "false");
  debugf("analyze_tztz: sign, index=>'%s'\n\n", index);
  
  int utc_offset_hours = atoi(index);
  debugf("utc_offset_hours=%d\n", utc_offset_hours);
  index = strstr(index, (char *) ":");
  debugf("after find ':', index=>%s\n\n", index);
  int utc_offset_minutes = atoi(++index);

  utc_offset_sec = utc_offset_hours * 60 * 60 + utc_offset_minutes * 60;

  if (neg)
  {
    utc_offset_sec = -utc_offset_sec;
  }
  debugf("analyze_tztz: utc_offset_sec=%d, hours offset=%.2f\n",
         utc_offset_sec, (float) utc_offset_sec / 3600);

  return utc_offset_sec;
}

//--------------------------------------------------------
// Return the timezone offset in (long) seconds.
//     tz_name - string holding timezone name
// If the timezone name is empty, try getting the timezone
// through the public IP address.
//--------------------------------------------------------

long get_tz_offset(char *tz_name)
{
  long tz_offset = 0;                   // default, "error" return value
  char url_buffer[128];                 // buffer to create URL in

  debugf("get_tz_offset: tz_name='%s'\n", tz_name);

  // if timezone configured, use it, otherwise IP lookup
  if (strlen(tz_name) == 0)
  {
    strcpy(url_buffer, IpLookupUrl);
  }
  else
  {
    sprintf(url_buffer, TzLookupUrl, tz_name);
  }
  
  debugf("get_tz_offset: url_buffer='%s'\n", url_buffer);

  // start the HTTP session
  http.begin(client, url_buffer);

  while (1)
  {
    // Send HTTP GET request
    int response = http.GET();

    debugf("get_tz_offset: response=%d\n", response);

    if (response == HTTP_CODE_OK)
    {
      char *payload = (char *) http.getString().c_str();
      
      if (strlen(tz_name) == 0)
      {
        tz_offset = analyze_iptz(payload);
      }
      else
      {
        tz_offset = analyze_tztz(payload);
      }
      
      tz_offset += NtpFudge;
      
      break;
    }

    if (response == HTTP_CODE_NOT_FOUND)
    {
      disp_flash_boot_stage();
      ESP.restart();
    }

    delay(1000);
  }

  // Free resources
  http.end();

  return tz_offset;
}

//--------------------------------------------------------
// Initialize the WiFi.
//
//     boot_stage  the boot stage we are at, so we can "twiddle"
//                 the LED for that stage on waiting for connect
// 
// Return 'true' if all OK, else return 'false'.
// Have a timeout on attempts to connect.
//--------------------------------------------------------

bool init_wifi(void)
{
  WiFi.begin(ClockData.SSID, ClockData.Password);
  debugf("Connecting to '%s'.", ClockData.SSID);

  while (WiFi.status() != WL_CONNECTED)
  {
    debugf(".");
    disp_flash_boot_stage();
  }

  debugf("done\n");

  return true;
}

void end_wifi(void)
{
  WiFi.mode(WIFI_OFF);
}

//--------------------------------------------------------
// Dump EEPROM data to the console.
//--------------------------------------------------------

void dump_eeprom_data(void)
{
  debugf("\nEEPROM data:\n");
  debugf("  SSID='%s'\n", ClockData.SSID);
  if (strlen(ClockData.Password) == 0)
  {
    debugf("  Password=''\n");
  }
  else
  {
    debugf("  Password=<is set>\n");
  }
  debugf("  Timezone='%s'\n", ClockData.Timezone);
  debugf("  BootHour=%02d\n", ClockData.BootHour);
  debugf("  Clockface='%c'\n", ClockData.Clockface);
  debugf("  Calendar='%c'\n", ClockData.Calendar);
  debugf("  ShowDate=%s\n", (ClockData.ShowDate) ? "true" : "false");
  debugf("  FirstDoWSunday=%s\n", (ClockData.FirstDoWSunday) ? "true" : "false");
  debugf("  Brightness=%d\n", ClockData.Brightness);
  debugf("  Debug=%s\n\n", (ClockData.Debug) ? "true" : "false");
}

//--------------------------------------------------------
// Check that the EEPROM config is valid.
//
// Returns "true" if valid, else "false".
//--------------------------------------------------------

bool config_valid(void)
{
  // check SSID
  if (strlen(ClockData.SSID) == 0)
  {
    strcpy(ErrorMessage, "Missing SSID");
    debugf("%s\n", ErrorMessage);
    return false;
  }

  // check SSID password
  if (strlen(ClockData.Password) == 0)
  {
    strcpy(ErrorMessage, "Missing password");
    debugf("%s\n", ErrorMessage);
    return false;
  }

  // check BootHour, NightStartHour, NightEndHour, Clockface
  if (! (ClockData.BootHour == 23 || (0 <= ClockData.BootHour && ClockData.BootHour <= 6)))
  {
    sprintf(ErrorMessage, "Bad BootHour:   %02d", ClockData.BootHour);
    debugf("%s\n", ErrorMessage);
    return false;
  }

  if (ClockData.Calendar != 'G' && ClockData.Calendar != 'B')
  {
    sprintf(ErrorMessage, "Bad Calendar:   %c", ClockData.Calendar);
    debugf("%s\n", ErrorMessage);
    return false;
  }

  if (! (MinBrightness <= ClockData.Brightness && ClockData.Brightness <= MaxBrightness))
  {
    sprintf(ErrorMessage, "Bad Brightness: %d", ClockData.Brightness);
    debugf("%s\n", ErrorMessage);
    return false;
  }

  return true;
}

//--------------------------------------------------------
// If CONFIG button still pressed, flash display until released.
//--------------------------------------------------------

void release_config(void)
{
//  disp_clear();

  // Wait for the button to be released
  while (digitalRead(ConfigButton) == LOW)
  {
    disp_flash_boot_stage();
    
//    disp_flash_status(1, STATUS_CONFIG_WAIT, STATUS_CONFIG_WAIT2);
//    disp_status(STATUS_CONFIG_WAIT);
//    delay(100);
//    disp_status(STATUS_CONFIG_WAIT);
//    delay(100);
////    ESP.wdtFeed();
  }
}

//--------------------------------------------------------
// Check if there is any reason we should go into CONFIG mode
//     was_pushed  "true" if button pushed at startup
//
// We check:
//     was_pushed
//     CONFIG button is down
//     The config data in ClockData is present and valid
//     init_wifi() always hangs until connected, no longer returns false
//
// Returns if all is OK.
//--------------------------------------------------------

void check_config(bool was_pushed)
{
  if (was_pushed || !config_valid() || !init_wifi())
  {
    // show that we are configuring
    disp_clear();
    disp_status(STATUS_CONFIG);
    disp_show();

    // start the config server, loop here forever servicing the server
    init_config_server();
    while (1)
    {
      server.handleClient();
    }

    // NEVER REACHED
  }
}
  
//********************************************************
// Initialize everything.
//********************************************************

void setup()
{
  // set mode of the config button pin, remember if button was down
  // do first so we don't have to hold button all during any debug wait
  pinMode(ConfigButton, INPUT_PULLUP);
  bool config_pushed = (digitalRead(ConfigButton) == LOW);

  // create the program name plus version string in a global buffer
  sprintf(ProgNameVersion, "%s %d.%d", ProgName, VERSION_MAJOR, VERSION_MINOR);
  
  // initialize the display, set brightness
  // do this early so disp_debug() shows something
  disp_begin();
  disp_set_pin(DispDin);  // must tell display the Din pin to use
  disp_face(ClockData.Clockface);
#if DISPLAY_TEST > 0
  disp_brightness(1);     // low brightness
  disp_test(600);
  disp_clear();
#endif

  // prepare the EEPROM and refresh in-memory copy
  EEPROM.begin(512);
  restore_eeprom_data();
  
  // if debugging display program name/version early
  // wait until sure debug prints will show
  if (ClockData.Debug)
  {
    // start serial 
    Serial.begin(115200);
    delay(1500);
  
    // announce program plus version
    printf("\n%s\n", ProgNameVersion);
  }

#if SHOW_RESTART_REASON > 0
  // display the restart reason
  show_restart_reason();
#endif

  // initialize the light sensor
  ambient_begin(LightPin);
  
  // set new display brightness
  disp_brightness(ambient_read());

  // initialize the orientation sensor, set display orientation
  orient_begin(AccelSda, AccelScl);
  disp_orientation(orient_read());

  // see if we need to go into "config" mode
  // if not, show booting plus stage
//  release_config();
  
  disp_status(STATUS_BOOT);
  disp_show();
  disp_boot_stage(0);
  check_config(config_pushed);
  
  // if we get here, normal operation
  // get timezone offset from the 'net
  TZOffsetSec = get_tz_offset(ClockData.Timezone);
  disp_boot_stage(1);

  // get NTP time from the 'net
  timeClient.setPoolServerName(NTPServer);
  timeClient.begin();
  timeClient.setUpdateInterval(NTPUpdateInterval);
  timeClient.update();
  disp_boot_stage(2);

  debugf("timeClient.getEpochTime()=%ld\n", timeClient.getEpochTime());
  
  // check if we got epoch time close to zero, retry if so
  // happens now and then, don't know why
  while (timeClient.getEpochTime() < 1000)  // is epoch WAY in the past?
  {
    // quick flashes of the "boot" display
    debugf("Bad time from NTP server, retrying...\n");
    disp_flash_boot_stage();
    
    // try getting the time again
    timeClient.update();
    debugf("retry: timeClient.getEpochTime()=%ld\n", timeClient.getEpochTime());

    delay(2000);
  }
  disp_boot_stage(3);

  // apply TZ offset only AFTER getting a good time
  timeClient.setTimeOffset(TZOffsetSec);
  timeClient.update();

#if NTP_UPDATE > 0
  // if NTP_UPDATE is non-zero turn off WiFi
  // if WiFi left on time will update every NTPUpdateInterval
  end_wifi();
#endif

  // clear the display, ready to go
  disp_clear();
  disp_boot_stage(4);
  delay(200);         // wait a bit to briefly show last boot stage

  // announce version, if not done above
  if (!ClockData.Debug)
  {
    printf("%s\n", ProgNameVersion);
  }
}

//********************************************************
// Update the clock display:
// * every second update display
// * in the middle of the minute show year, date, DoW, if configured
// * every 24 hours reboot to pick up TZ/daylight savings data
//********************************************************

void loop()
{
  static int prev_second = -1;
  static int prev_minute = -1;

  // get current time
  int seconds = timeClient.getSeconds();
  int minutes = timeClient.getMinutes();
  int hours = timeClient.getHours();

  // update display every new second
  if (seconds != prev_second)
  {
    // set flag for next "seconds" update
    prev_second = seconds;

    // update the time, only actually updates every NTPUpdateInterval period
    timeClient.update();

    // set new display brightness
    disp_brightness(ambient_read());

    // check orientation
    disp_orientation(orient_read());

    // clear the display
    disp_clear();

    // get current time into a struct
    time_t rawtime = timeClient.getEpochTime();
    struct tm *now_time = localtime(&rawtime);

    // if it's the boot hour, minutes are 0 and seconds are small
    if (hours == ClockData.BootHour && minutes == 0 && seconds < 5)
    {
      // reboot
      ESP.restart();
      
      // never reached
    }

    // every minute check WiFi, attempt reconnect if necessary
    if ((minutes != prev_minute) && (WiFi.status() != WL_CONNECTED))
    {
      // set flag for next "minutes" update
      prev_minute = minutes;

      // try to reconnect
      WiFi.disconnect();
      WiFi.begin(ClockData.SSID, ClockData.Password);
    }

    // if seconds in [24:36] show year/date/DoW
    if (ClockData.ShowDate && (24 <= seconds && seconds < 36))
    {
      if (seconds < 28)
      {
        // show YEAR
        int year = now_time->tm_year + 1900;

        if (ClockData.Calendar == 'B')
        {
          year += 543;    // Thai Buddhist calendar year is Gregorian+543
        }

        disp_status(STATUS_YEAR);
        disp_year(year);
      }
      else if (seconds < 32)
      {
        // show DATE - mm:dd
        disp_status(STATUS_DATE);
        disp_date(now_time->tm_mon + 1, now_time->tm_mday);
      }
      else
      {
        // show DoW
        disp_status(STATUS_DOW);
        disp_dow(now_time->tm_wday, ClockData.FirstDoWSunday);
      }
    }
    // otherwise show the time
    else
    {
      disp_status(STATUS_CLOCK);
      disp_time(hours, minutes, seconds);
    }
    
    disp_show();
    debugf("%02d:%02d:%02d\n", hours, minutes, seconds);
  }
}
