CryptiClock
===========

Yet another clock based on an ESP8266, but with a "cryptic" 8x8 LED display.

Features
--------

The clock has a display of 8x8 WS2812B LEDs to display the "cryptic" time as:

* Binary numbers, or
* `Cistercian numerals <https://en.wikipedia.org/wiki/Cistercian_numerals>`_, or
* Extended Die faces, (0..9).

The initial idea was the clock should hang off the wall or be placed
on a desktop.  Since there is usually something on the desk that interferes with
power connectors of objects, there is an MPU6050 accelerometer in the clock to
ensure the display is always the right way up no matter how the clock is
oriented.

The clock keeps time by periodically synchronizing with an NTP server over
the local network.

Every morning the clock checks a timezone database to keep track of daylight
saving changes.

There are a multitude of configuration parameters stored in EEPROM.  The
configuration process is started by holding the CONFIG button, applying power
and maintaining the button press until the display flashes. Then connect
your computer to the "crypticlock" open access point and you will be able to
change the settings shown:

* SSID
* Password
* Timezone (automatic if not set)
* Clockface
* Calendar to use
* Reboot hour (to check daylight saving)

and many others

When ready press the "Save" button.

Schematic
---------

The schematic for version 2.5 of the clock is in the *CryptiClock_Schematic.pdf* file.

.. image:: CryptiClock_Schematic.jpg


Final assembly of the 2.5 version
---------------------------------

The assembled v2.5 board looks like this:

.. image:: CryptiClock_Assembly_2.5.jpg

Configuration Page
------------------

If the firmware determines during boot that the configuration has never been
set, or the config button SW1 is held down, the ESP8266 configures itself as
an Access Point with the name of "crypticlock".  You connect a phone, tablet
or other computer to that AP (no password) and 
you will see the configuration page:

.. image:: CryptiClock_ConfigPage.jpg

The SSID dropdown shows the user a list of available WiFi access points in
the area.

After setting the configuration to the correct values the "Save and Reboot"
button signals the clock to save the new data and reboot.

If the "OTA upload" checkbox is set the clock will boot into an *over-the-air*
state which allows the Arduino IDE to load the code into the clock via WiFi.

If the "Timezone" field is left empty the clock tries to determine the 
timezone from the public address of the local WiFi.  If this is incorrect
for some reason you can set the timezone to whatever you want which overrides
the automatic timezone determination.  The
`list of allowed timezone strings is here <http://worldtimeapi.org/api/timezone.txt>`_.

There are three "clockfaces" that can be used.  This is the "die" clockface showing 
13:12:

.. image:: Die_Clockface.JPG

This is the "binary" clockface showing 13:14:

.. image:: Binary_Clockface.JPG

This is the "cistercian" clockface showing 13:15:

.. image:: Cistercian_Clockface.JPG

Laser-cut case
--------------

I finally found a place that could cut acrylic sheet, and made up a case
design for 2mm transparent grey sheet.  These files are the *CryptiClock_Case.\**
files.  Once cut the bits went together surprisingly well for only my second
attempt at this sort of thing.  The only thing I might change is to use 3mm
sheet next time - there's a little bit too much flexing in the front cover sheet.
I put a sheet of thick white paper between the LEDs and front cover to
diffuse the display a bit and reduce brightness.  A grid spacer was also 
designed to give a more pleasing visual effect.

A short video showing the completed clock in action with the "die" clockface
is in the
`CryptiClock_finished.m4v <https://gitlab.com/rzzzwilson/crypticlock/-/blob/main/CryptiClock_finished.m4v>`_
file.  Also shown is a clock kit I assembled some years ago from an AliExpress
kit.  I blame that kit for this new clock!


Sources of inspiration
----------------------

Original idea from this project:

https://www.instructables.com/Cistercian-Digital-Clock/


Setup
=====

To build the software with the Arduino IDE:

* install the board data from "http://arduino.esp8266.com/stable/package_esp8266com_index.json"
* select "ESP8266 12-E module" as the board
* install the "MPU6050" library from Electronic Cats
* install the "NTPClient" library by Fabrice Weinberg
* install the "Adafruit Neopixel" library by Adafruit

