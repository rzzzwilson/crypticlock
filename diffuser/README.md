# Diffuser grid for CryptiClock

An experimental grid to try to make the LEDs in the clock display
square.

The actual cutting lines must be drawn in the "Edge Cuts" layer only.  Get DXF
data from the project with *kicadpcb2dxf.py*.

For example:

    python kicadpcb2dxf.py -f diffuser.kicad_pcb
