A case for SmartClock
=====================

Try to use KiCad to design the cutting file.  The actual cutting lines
must be drawn in the "Edge Cuts" layer only.  Get DXF data from the 
project with *kicadpcb2dxf.py*.

For example:

    python kicadpcb2dxf.py case.kicad_pcb
